

class Pipeline:
    def __init__ (self):
        self.step=[]

    def add_step(self, new_step):
        self.step.append(new_step)
    
    def run(self):
        table= None
        for i in range(0, len(self.step)):
            table=self.step[i].process(table)
        return table




