from operations.transformers.chargement import Chargement
from operations.transformers.fenetrage import Fenetrage
from operations.transformers.selection_var import Selection_var
from operations.transformers.centrage import Centrage
from operations.transformers.normalisation import Normalisation
from operations.transformers.exportation import Exportation
from operations.transformers.moyenne_glissante import MoyenneGlissante
from operations.estimators.moyenne import Moyenne
from operations.estimators.variance import Variance
from operations.estimators.ecart_type import Ecart_type
from operations.estimators.k_means import K_means
from table import Table
from pipeline import Pipeline

if __name__ == "__main__":
    pipe1=Pipeline()

    pipe1.add_step(Chargement('/Users/cecil/OneDrive/cours Ensai/projet info/données/Données/', 'covid-hospit-incid-reg-2021-03-03-17h20.csv', "csv"))
    table1=pipe1.run()
    print(table1.header)
    print(table1.body[0:5])
    print(len(table1.body))

    pipe1.add_step(Fenetrage('2020-03-19', '2020-03-19'))
    table2=pipe1.run()
    print(table2.header)
    print(table2.body)
    print(len(table2.body))

    pipe1.add_step(Selection_var(['incid_rea']))
    table4=pipe1.run()
    print(table4.header)
    print(table4.body)
    print(len(table4.body))

    pipe1.add_step(MoyenneGlissante(7))
    table5=pipe1.run()
    print(table5.header)
    print(table5.body)
    print(len(table5.body))




    #pipe1.add_step(K_means(3, 'nomReg', 15))
    #table3=pipe1.run()
    #print(table3.header)
    #print(table3.body)
    #print(len(table3.body))

    #pipe1.add_step(Normalisation())
    #table4=pipe1.run()
    #print(table4.header)
    #print(table4.body)
    #print(len(table4.body))

    #pipe1.add_step(Exportation('test_PTD'))
    #pipe1.run()


