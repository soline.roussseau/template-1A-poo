class Moyenne (Estimators):
    """Calcul la moyenne de chaque variable de la colonne i.
    
    La moyenne d'une variable se calcule en additionnant toutes ses valeurs et en divisant la somme par
    le nombre de valeurs

    Parameters
    ----------
    table : type1
       tableau de données
    
    Returns
    -------
    list
        la valeur retournée est une liste des moyennes de chaque colonne i.

    Examples
    --------
    >>> calculer_moyenne(1, 2, 3)
    2

    >>> calculer_moyenne(3, 2, 1)
    2
        
    """
    def__init__(self)
    def calculer_moyenne(self,table)
        somme = 0
        moyenne = []
        list = ()
        for i in table.header : 
            nb_ligne = length(table.body[i])
            for j in nb_ligne :
                somme += table.body[i,j]
            moyenne[i] = somme / nb_ligne
            list.appened(moyenne[i])
        return list
        
        
        
