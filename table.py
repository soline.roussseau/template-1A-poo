class Table:
    """Definir les objets sous forme de table

    Un objet de type Table est composé de listes pour chacune des lignes du tableau

    Attributs
    ----------
    header : list
        liste constituée des noms de chaque variables (première ligne du tableau)
    body : list
        liste qui contient une liste pour chaque ligne du tableau (sauf la première ligne)

    Méthodes
    ----------
    add_col(column, position)
        permet d'ajouter une colonne au tableau. Le paramètre column est une liste qui contient les observations à ajouter
        et le paramètre position est un entier qui indique l'endroit où ajouter la colonne.
    
    remove_col(position)
        permet de supprimer une colonne du tableau. Le paramètre position est un entier qui correspond à l'endroit où
        sera supprimée la colonne.

    add_row(row, position)
        permet d'ajouter une ligne au tableau. Le paramètre row est une liste qui contient les observations à ajouter 
        et le paramètre position est un entier qui indique l'endroit où ajouter la ligne.
    
    remove_row(position)
        permet de supprimer une ligne du tableau. Le paramètre position est un entier qui correspond à l'endroit où
        sera supprimée la ligne.
    """
    def __init__(self, header, body):
        self.header = header
        self.body = body
    
    def add_col(self, column, position=-1):
        self.header.insert(position, column[0])
        for i in range(0, len(self.body)):
            self.body[i].insert(position, column[i+1])

    def remove_col(self, position):
        del(self.header[position])
        for i in range(0, len(self.body)):
            del(self.body[i][position])

    def add_row(self, row, position=-1):
        self.body.insert(position, row)

    def remove_row(self, position=-1):
        del(self.body[position])
