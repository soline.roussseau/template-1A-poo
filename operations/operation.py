from abc import ABC, abstractmethod

class Operation(ABC):
    def __init__(self) -> None:
        pass

    @abstractmethod
    def process(self, table):
        pass
