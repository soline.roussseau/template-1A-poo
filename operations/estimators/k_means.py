from operations.estimators.estimators import Estimators
from table import Table
import random



def distance_eucli(pointA, pointB):
    somme = 0
    for i in range(len(pointA)):
        somme += (float(pointA[i])-float(pointB[i]))**2
    return pow( somme , 0.5)


class K_means (Estimators):
    """Réalise une classification par kmeans

    Permet de classer plusieurs individus définis par une variable qualitative en K classes selon l'algorithme K-means
    (prend en compte les distances euclidiennes de chaque observations avec des barycentres définis d'abord aléatoirement
    puis définis selon les classes crées à l'itération précédentes)

    Parameters
    ----------
    table : Table
         tableau de données dont le body contient une ou plusieurs variables quantitatives et une seule variable 
         qualitative.
  
    Attributs
    ----------
    nb_classes: int
        correspond au nombre de classes choisies par l'utilisateurs pour réaliser le kmeans

    var_quali: str
        l'utilisateur indique quelle est la variable qualitative dans le tableau (elle servira de nom aux individus et
        apparaitra dans le résultat de la classification).

    max_iterations: int
        l'utilisateurs choisie le nombre d'itérations de l'algorithme qui doit être suffisament élevé (environ une 
        centaine).

    Returns
    -------
    Table
        Table dont le header est vide et dont le body contient une liste pour chaque classe. Chaque liste du body 
        contient les noms des individus regroupés dans la classe (le nom est attibué selon la variable qualitative
        choisie par l'utilisateur).

    Examples
    --------
    table1 est une table qui contient les données du fichier "covid-hospit-incid-reg-2021-03-03-17h20.csv"
    pour le 19 mars 2020 avec seulement les variables "nomReg" et "incid_rea"

    >>>table2=K_means(3, "nomReg", 100).fit(table1)
    >>>print(table2.header)
    >>>print(table2.body)

    []
    [['Auvergne-Rhône-Alpes', 'Bourgogne-Franche-Comté', 'Grand-Est', 'Hauts-de-France', 'Occitanie', 
    "Provence-Alpes-Côte d'Azur"], ['Ile-de-France'], ['Bretagne', 'Centre-Val de Loire', 'Corse', 
    'Guadeloupe', 'Guyane', 'La Réunion', 'Martinique', 'Mayotte', 'Normandie', 'Nouvelle-Aquitaine', 
    'Pays de la Loire']]
    """

    def __init__(self, nb_classes, var_quali, max_iterations):
        super().__init__()
        self.cl = nb_classes
        self.quali = var_quali
        self.iterations = max_iterations



    def fit(self, table):
        num_col = table.header.index(self.quali)
        liste_indiv = []
        for i in range(0, len(table.body)):
            liste_indiv.append(table.body[i][num_col])
        table.remove_col(num_col)

        col = []
        for i in range(len(table.header)):
            col.append([0])
        for i in range(0, len(table.body)):
            for j in range(0, len(table.header)):
                col[j].append(float(table.body[i][j]))

        liste_centres=[]
        for n in range(self.cl):
            centre_cl=[]
            for j in range(len(table.header)):
                centre_cl.append(random.randint(min(col[j]), max(col[j])+1))
            liste_centres.append(centre_cl)
        
        
        compteur=0
        while compteur<self.iterations:
            compteur += 1
            cluster=[]
            valeurs_cluster=[]
            for i in range(self.cl):
                cluster.append([])
                valeurs_cluster.append([])

            for i in range(len(table.body)):
                distance=[]
                for barycentre in liste_centres:
                    distance.append(distance_eucli(table.body[i], barycentre))
                cluster[distance.index(min(distance))].append(liste_indiv[i])
                valeurs_cluster[distance.index(min(distance))].append(table.body[i])

            liste_centres=[]
            for classe in valeurs_cluster:
                somme = []
                for i in range(len(table.header)):
                    somme.append(0)
                for obs in classe:
                    for i in range(len(table.header)):
                        somme[i] += float(obs[i])
                for i in range(len(table.header)):
                    somme[i]= float(somme[i]) / float(len(classe))
                liste_centres.append(somme)

        return Table([], cluster)



            















