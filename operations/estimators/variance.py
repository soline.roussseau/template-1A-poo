from operations.estimators.estimators import Estimators
from table import Table

class Variance (Estimators):
    """Calcule les variances d'un jeu de données

    Ce calcul consiste à faire la somme des carrés des différences entre les valeurs de chaque
    données et leur moyenne. La variance permet ainsi de mesurer la dispersion des valeurs.

    Parameters
    ----------
    table : Table
       tableau de données dont le body contient uniquement des variables quantitatives

    Returns
    -------
    Table
        la valeur retournée est une table avec header qui correspond à la liste des noms des variables et en body
        les variances associées.

    Examples
    --------
    table1=Table(["var1","var2"],[[1,3],[2,3],[3,3]])
    >>> table2=Variance.fit(table1)
    >>>print(table2.header)
    >>>print(table2.body)

    (["var1","var2"])
    (0.66,0)
  
    """
    
    def __init__(self):
        super().__init__()

    def fit(self, table):
        variance = []
        nb_lignes=len(table.body)
        for i in range(0, len(table.header)) : 
            somme = 0
            for j in range(0, nb_lignes) :
                somme += int(table.body[j][i])
            moyenne = somme / nb_lignes
            somme = 0
            for j in range(0, nb_lignes):
                somme += (int(table.body[j][i]) - moyenne)**2
            calcul = somme / nb_lignes
            variance.append(calcul)
        return Table(table.header, variance)
