from abc import ABC, abstractmethod
from operations.operation import Operation

class Estimators(Operation):
    def __init__(self):
        super().__init__()
        pass

    def process(self, table):
        return self.fit(table)

    @abstractmethod
    def fit(self, table):
        pass
