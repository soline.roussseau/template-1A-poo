from operations.estimators.estimators import Estimators
from table import Table

class Ecart_type (Estimators):
    """Calcule les écart-type de chaque variable de colonne i

    Ce calcul consiste à calculer la racine de la somme des carrés des différences entre les valeurs de chaque
    données et leur moyenne. Il mesure donc la dispersion autour de la moyenne.

    Parameters
    ----------
    table : Table
       tableau de données dont le body contient uniquement des variables quantitatives

    Returns
    -------
    Table
        la valeur retournée est une table avec header qui correspond à la liste des noms des variables et en body
        les écart-types associés. 

    Examples
    --------
    table1=Table(["var1","var2"],[[1,3],[2,3],[3,3]])
    >>> table2=Ecart_type.fit(table1)
    >>>print(table2.header)
    >>>print(table2.body)

    (["var1","var2"])
    (0.8,0)
    """
    def __init__(self):
        super().__init__()

    def fit(self, table):
        ecart_type = []
        nb_lignes=len(table.body)
        for i in range(0, len(table.header)) : 
            somme = 0
            for j in range(0, nb_lignes) :
                somme += int(table.body[j][i])
            moyenne = somme / nb_lignes
            somme = 0
            for j in range(0, nb_lignes):
                somme += (int(table.body[j][i]) - moyenne)**2
            calcul = pow(somme / nb_lignes, 0.5)
            ecart_type.append(calcul)
        return Table(table.header, ecart_type)
