from operations.estimators.estimators import Estimators
from table import Table

class Moyenne (Estimators):
     """Calcul la moyenne de chaque variable de la colonne i.
    
    La moyenne d'une variable se calcule en additionnant toutes ses valeurs et en divisant la somme par
    le nombre de valeurs

    Parameters
    ----------
    table : Table
       tableau de données dont le body contient uniquement des variables quantitatives
    
    Returns
    -------
    Table
        la valeur retournée est une table avec header qui correspond à la liste des noms des variables et en body
        les moyennes associées.
    Examples
    --------
    
    table1=Table(["var1","var2"],[[1,3],[2,3],[3,3]])
    >>> table2=Moyenne.fit(table1)
    >>>print(table2.header)
    >>>print(table2.body)

    (["var1","var2"])
    (2,3)
  
    """
     def __init__(self):
        super().__init__()

     def fit(self, table):
        moyenne = []
        nb_lignes=len(table.body)
        for i in range(0, len(table.header)) : 
            somme = 0
            for j in range(0, nb_lignes) :
                somme += int(table.body[j][i])
            calcul = somme / nb_lignes
            moyenne.append(calcul)
        return Table(table.header, moyenne)
        
        
        
