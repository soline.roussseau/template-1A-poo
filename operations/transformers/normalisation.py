from operations.transformers.transformers import Transformers
from table import Table

class Normalisation(Transformers) : 

    ''' normalisation

    La normalisation consiste à soustraire la moyenne et à diviser par l'écart-type. 
    Dans ce cas, chaque valeur reflète la distance par rapport à la moyenne en unité d'écart-type.
    Cette méthode est utilisée pour que les variables, mesurées à différentes échelles, 
    aient des valeurs comparables.   
    
    Parameters
    ----------
    table : Table
        tableau de données dont le body contient uniquement des variables quantitatives
    
    Returns
    -------
    table
        Retourne la table composée des valeurs normalisées.

    Examples
    --------
    table1=Table(["var1","var2"],[[1,3],[2,3],[3,3]])
    >>> table2=Normalisation.fit(table1)
    >>>print(table2.header)
    >>>print(table2.body)

    (["var1","var2"])
    ([-1.25,0],[0,0],[1.25,0])'''

    def __init__(self):
        super().__init__()

    def transform(self, table):
        nb_lignes = len(table.body)
        liste_moy = []
        liste_ecart_type = []
        nouv_body=[]
        for i in range(0, len(table.header)):
            somme = 0
            for j in range(0, nb_lignes):
                somme += int(table.body[j][i])
            moyenne = somme / nb_lignes
            liste_moy.append(moyenne)
            somme = 0
            for j in range(0, nb_lignes):
                somme += (int(table.body[j][i]) - moyenne)**2
            ecart_type = pow(somme / nb_lignes, 0.5)
            liste_ecart_type.append(ecart_type)
        for row in table.body:
            row_nouv_body=[]
            for i in range(0, len(table.header)):
                nouv_valeur=(int(row[i])-liste_moy[i])/liste_ecart_type[i]
                row_nouv_body.append(nouv_valeur)
            nouv_body.append(row_nouv_body)
        return Table(table.header, nouv_body)
