from operations.transformers.transformers import Transformers
from table import Table
import csv
import json

class Chargement(Transformers):
    """Permet de charger les données.

    Transforme un fichier csv ou json contenu dans l'ordinateur de l'utilisateur en un objet de type Table.

    Attributs
    ----------
    folder : str
        nom du dossier dans lequel est contenue le fichier
    filename : str
        nom du fichier
    type_fichier : str
        correspond au type de fichier. Il peut s'agir d'un fichier de type csv ou json

    Returns
    -------
    Table
        la valeur retournée est une table des données du fichier.

    """
    def __init__(self, folder, filename, type_fichier):
        super().__init__()
        self.folder=folder
        self.filename=filename
        self.type_fichier=type_fichier
    
    def transform(self, table):
        data=[]
        if self.type_fichier=="csv":
            with open(self.folder + self.filename, encoding='ISO-8859-1') as csvfile :
                covidreader = csv.reader(csvfile, delimiter=';')
                for row in covidreader :
                    data.append(row)
        if self.type_fichier=="json":
            with open(self.folder + self.filename) as json_file :
                data = json.load(json_file)
        header=data[0]
        del(data[0])
        body=data
        return Table(header, body)
