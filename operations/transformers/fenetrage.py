from operations.transformers.transformers import Transformers
from table import Table
from datetime import datetime

class Fenetrage(Transformers):
    """Sélection de données sur une période donnée.

    Transforme le jeu de données en sélectionnant celles qui correspondent à une période donnée.

    Parameters
    ----------
    date_debut : str
        Date du début de la période à sélectionner (doit être au même format que les dates présentes
        dans le jeu de données)
    date_fin : str
        Date de la fin de la période à sélectionner (doit être au même format que les dates présentes
        dans le jeu de données)
    
    Returns
    -------
    table
        retourne la nouvelle table composée des données de la période sélectionnée.

    """
    def __init__(self,  date_debut, date_fin):
        super().__init__()
        self.debut = datetime.strptime(date_debut , "%Y-%m-%d")
        self.fin = datetime.strptime(date_fin , "%Y-%m-%d")
    
    def transform(self, table):
        num_col = table.header.index("jour")
        new_body=[]
        for row in table.body:
            if self.debut <= datetime.strptime(row[num_col], "%Y-%m-%d") <= self.fin:
                new_body.append(row)
        return Table(table.header, new_body)
                
        










