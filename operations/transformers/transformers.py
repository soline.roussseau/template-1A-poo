from abc import ABC, abstractmethod
from operations.operation import Operation

class Transformers(Operation):
    def __init__(self):
        super().__init__()
        pass

    def process(self, table):
        return self.transform(table)

    @abstractmethod
    def transform(self, table):
        pass
