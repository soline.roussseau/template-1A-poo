from operations.transformers.transformers import Transformers
from table import Table

class Selection_var(Transformers):
     """Transforme un jeu de données en ne sélectionnant que certaines variables.

    Sélectionne les données de variables spécifiées.

    Parameters
    ----------
    Table : table
        jeu de données 

    Attributs
    ----------
    var_a_selectionner : list
        liste des variables que l'on souhaite conserver (même lorsque l'on souhaite conserver qu'une seule variable, on
        la met dans une liste)
    
    Returns
    -------
    Table
        retourne la nouvelle table composée des variables sélectionnées.

    """
     def __init__(self,  var_a_selectionner):
        super().__init__()
        self.var=var_a_selectionner
    
     def transform(self, table):
        nouv_header=[]
        nouv_body=[]
        num_col_keep=[]
        for i in table.header:
            for j in self.var:
                if i==j:
                    nouv_header.append(i)
                    num_col_keep.append(table.header.index(i))
        for row in table.body:
            row_nouv_body=[]
            for i in num_col_keep:
                row_nouv_body.append(row[i])
            nouv_body.append(row_nouv_body)
        return Table(nouv_header, nouv_body)



