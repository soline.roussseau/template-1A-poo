from operations.transformers.transformers import Transformers
from table import Table
"""Calcule la moyenne glissante sur un jeu de données.

    Permet de calculer la moyenne glissante sur un nombre de jours donné.

    Parameters
    ----------
    table : Table
        tableau de données dont le body contient uniquement des variables quantitatives
    
    Attributs
    ----------
    intervalle_tps : int
        correspond au nombre de jours sur lesquels est calculée la moyenne

    Returns
    -------
    Table
        retourne une table identique à celle d'origine mais à laquelle une colonne corespondant aux moyennes glissantes
        est rajoutée.

    """


class MoyenneGlissante(Transformers) :
    def __init__(self, intervalle_tps):
        super().__init__() 
        self.intervalle_tps = int(intervalle_tps)
    
    def transform(self, table) : 
        moy = [None for i in range(len(table.body))]
        for i in range (self.intervalle_tps - 1, len(table.body)) :
            sum = 0
            for j in range(0, self.intervalle_tps) : 
                sum += int(table.body[i-self.intervalle_tps + j + 1][0])
            moy[i]=sum/self.intervalle_tps
        moy.insert(0, "moyenne_glissante")
        table.add_col(moy) 
        return table


