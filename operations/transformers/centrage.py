from operations.transformers.transformers import Transformers
from table import Table

class Centrage(Transformers) : 

    ''' Centrage

    Centrer une variable consiste à soustraire son espérance à chacune de ses valeurs initiales, 
    soit retrancher à chaque donnée la moyenne de la variable.

    Parameters
    ----------
    table : Table
       tableau de données dont le body contient uniquement des variables quantitatives

    Returns
    --------
    Table
        tableau dont la structure est identique au tableau d'entrée.

    examples
    --------
    table1=Table(["var1","var2"],[[1,3],[2,3],[3,3]])
    >>> table2=Centrage.fit(table1)
    >>>print(table2.header)
    >>>print(table2.body)

    (["var1","var2"])
    ([-1,0],[0,0],[1,0])

    '''
    def __init__(self):
        super().__init__()

    def transform(self, table):
        nouv_body=[]
        nb_lignes=len(table.body)
        moyenne=[]
        for i in range(0, len(table.header)) : 
            somme=0
            for j in range(0, nb_lignes) :
                somme += int(table.body[j][i])
            moyenne.append(somme / nb_lignes)
        for row in table.body:
            row_nouv_body = []
            for i in range(0, len(table.header)):
                row_nouv_body.append(int(row[i]) - moyenne[i])
            nouv_body.append(row_nouv_body)
        return Table(table.header, nouv_body)
        