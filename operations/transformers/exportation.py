from operations.transformers.transformers import Transformers
from table import Table
import csv
 
class Exportation(Transformers):
    """Permet d'exporter un tableau de données.

    Transforme les tables en un fichier csv.

    Parameters:
    ----------
    table : Table
        tableau de données
        
    Attributs
    ----------
    nom_fichier : str
        nom du fichier qui sera exporté (doit se finir par ".csv")
    
    Returns
    -------
    fichier csv dont les valeurs sont séparées par une virgule.

    """

    def __init__(self, nom_fichier):
        super().__init__()
        self.nom=nom_fichier
    
    def transform(self, table):
        with open(self.nom, 'w', newline='') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',', quotechar=',', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(table.header)
            for i in range(0, len(table.body)):
                spamwriter.writerow(table.body[i])

    